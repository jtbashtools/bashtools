#! /usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Vérifie que le projet est installé
# Arguments:
#   $1 -> Chemin du projet
# Error:
#   ErrorNotExistDirectory -> Si le chemin n'éxiste pas
#   ErrorNotBashtoolsProject -> Si le projet n'est pas un bashtools projet
#   ErrorProjectAlreadyInstalled -> Si le projet est déjà installé
# Since: 2.0.0
##################################################################
function check_is_installed ()
{
    # On récupère le chemin du projet
    path_project=${1:-}

    # On vérifie que le chemin existe bien
    directory_exist "${path_project:-}"

    # On vérifie que le projet est bien un projet bashtools
    is_bashtools_project "${path_project}"

    # On récupère le nom du projet
    project_name=`get_param name ${path_project}/${FILE_BASHTOOLS}`

    #On test si le projet est déja installé
    if ls ${PATH_SAVE_DIRECTORY} | egrep "^${project_name}@.*" >> /dev/null
    then
        >&2 echo "ErrorProjectAlreadyInstalled : The project is already installed"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Vérifie les dépendances du projet
# Arguments:
#   $1 -> Chemin du projet
# Error:
#   ErrorArgNull -> Si le chemin est null
#   ErrorNotExistDirectory -> Si le chemin n'éxiste pas
#   ErrorNotBashtoolsProject -> Si le projet n'est pas un bashtools projet
# Since: 2.0.0
##################################################################
function check_dependances ()
{
    # On récupère le chemin du projet
    path_project=${1:-}

    # On vérifie que le chemin existe bien
    directory_exist "${path_project:-}"

    # On vérifie que le projet est bien un projet bashtools
    is_bashtools_project "${path_project}"

    # On vérifie que les dépendances sont déja installées
    dependances=`get_param "dependances" ${path_project}/${FILE_BASHTOOLS}`
    dependances_missing=()
    for dependance in ${dependances[@]:-}
    do
        if ! command -v ${dependance} > /dev/null
        then
            dependances_missing+=(${dependance})
        fi
    done

    # Si il y a des dépendances manquantes, on affiche une erreur
    if [[ ${#dependances_missing[@]} -gt 0 ]];
    then
        echo "Erreur, les dépendances suivantes sont manquante :"
        echo "${dependances[@]}"
        exit 1
    fi

    return 0
}

##################################################################
# Purpose: Sauvegarde le projet dans le dossier des projets bashtools
# Arguments:
#   $1 -> Chemin du projet
# Error:
#   ErrorNotRootUser -> Si l'utilisateur n'est pas root
#   ErrorArgNull -> Si le chemin est null
#   ErrorNotExistDirectory -> Si le chemin n'éxiste pas
#   ErrorNotBashtoolsProject -> Si le projet n'est pas un bashtools projet
# Since: 2.0.0
##################################################################
function save_project ()
{
    # On vérifie que l'utilisateur est root
    user_is_root

    # On récupère le chemin du projet
    path_project=${1:-}

    # On vérifie que le chemin existe bien
    directory_exist "${path_project:-}"

    # On vérifie que le projet est bien un projet bashtools
    is_bashtools_project "${path_project}"

    # On récupère les différentes informations du projet
    name_project=`get_param "name" ${path_project}/${FILE_BASHTOOLS}`
    version_project=`get_param "version" ${path_project}/${FILE_BASHTOOLS}`
    path_bin_project=`get_param "path_bin" ${path_project}/${FILE_BASHTOOLS}`

    # On vérifie que les informations obligatoire du projet ne sont pas null
    arg_is_not_null "${name_project:-}" "Don't find name variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${version_project:-}" "Don't find version variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${path_bin_project:-}" "Don't find path_bin_project variable in ${FILE_BASHTOOLS}."

    # Nom du projet à sauvegarder
    project_save=${name_project}@${version_project}

    # On rècupère le nom du fichier additionel de désinstallation
    uninstall_file=`get_param "uninstal_file" ${path_project}/${FILE_BASHTOOLS}`
    mkdir -p ${PATH_SAVE_DIRECTORY}/${project_save}

    # On enregistre les fichiers bin à supprimer lors de la désinstallation
    ls ${path_project}/bin/ > ${PATH_SAVE_DIRECTORY}/${project_save}/${FILE_TO_UNINSTALL}

    # On copie le fichier supplémentaire de désinstallation
    if [[ -f ${uninstall_file} ]];
    then
        cp ${path_project}/${uninstall_file} ${PATH_SAVE_DIRECTORY}/${project_save}
    fi

    # on copie le fichier bashtools.yml
    cp ${path_project}/${FILE_BASHTOOLS} ${PATH_SAVE_DIRECTORY}/${project_save}

    return 0
}

##################################################################
# Purpose: Installe le projet
# Arguments:
#   $1 -> Chemin du projet
#   $2 -> Mode
# Error:
#   ErrorNotRootUser -> Si l'utilisateur n'est pas root
#   ErrorArgNull -> Si le chemin est null
#   ErrorNotExistDirectory -> Si le chemin n'éxiste pas
#   ErrorNotBashtoolsProject -> Si le projet n'est pas un bashtools projet
# Since: 2.0.0
##################################################################
function install_project ()
{
    # On vérifie que l'utilisateur est root
    user_is_root

    # On récupère le chemin du projet
    path_project=${1:-}

    # On récupère le mode d'installation
    mode=${2:-}

    # On vérifie que le chemin existe bien
    directory_exist "${path_project:-}"

    # On vérifie que le projet est bien un projet bashtools
    is_bashtools_project "${path_project}"

    # On récupère les différentes informations du projet
    name_project=`get_param "name" ${path_project}/${FILE_BASHTOOLS}`
    version_project=`get_param "version" ${path_project}/${FILE_BASHTOOLS}`
    path_bin_project=`get_param "path_bin" ${path_project}/${FILE_BASHTOOLS}`
    path_lib_project=`get_param "path_lib" ${path_project}/${FILE_BASHTOOLS}`
    path_data_project=`get_param "path_data" ${path_project}/${FILE_BASHTOOLS}`
    install_file=`get_param "install_file" ${path_project}/${FILE_BASHTOOLS}`

    # On vérifie que les informations obligatoire du projet ne sont pas null
    arg_is_not_null "${name_project:-}" "Don't find name variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${version_project:-}" "Don't find version variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${path_bin_project:-}" "Don't find path_bin_project variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${path_lib_project:-}" "Don't find path_lib_project variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${path_data_project:-}" "Don't find path_data_project variable in ${FILE_BASHTOOLS}."

    # On crée les dossiers bin lib et data (si ils n'existent pas)
    mkdir -p ${path_bin_project}
    mkdir -p ${path_lib_project}
    mkdir -p ${path_data_project}

    # On rend executable les fichiers qui doivent etre executés
    chmod +x ${path_project}/bin/* \
        ${path_project}/lib/* 2> /dev/null || true

    # On install (soit en mode production ou en mode développement) les différents fichiers 
    if [[ ${mode} == 'prod' ]];
    then
        cp -rv ./bin/*  ${path_bin_project}/ || true
        cp -rv ./data/* ${path_data_project}/ 2> /dev/null || true
        cp -rv ./lib/* ${path_lib_project}/ 2> /dev/null || true
        cp -rv ./lib/${FILE_BUNDLE} ${path_lib_project}/
    else 
        echo "installation en mode dévellopement"
        ln -sv `pwd`/bin/* ${path_bin_project}/ || true
        ln -sv `pwd`/data/* ${path_data_project}/ 2> /dev/null || true 
        ln -sv `pwd`/lib/* ${path_lib_project}/ 2> /dev/null || true
        ln -sv `pwd`/lib/${FILE_BUNDLE} ${path_lib_project}/
    fi

    # On execute le fichier install supplémentaire si il existe
    if [[ -f ${install_file:-} ]];
    then
        chmod +x ${path_project}/${install_file}
        cd ${path_project} && ./${install_file} ${mode} && cd -
    fi

    return 0
}

##################################################################
# Purpose: Installe le fichier de configuration
# Arguments:
#   $1 -> Chemin du projet
# Error:
#   ErrorArgNull -> Si le chemin est null
#   ErrorNotExistDirectory -> Si le chemin n'éxiste pas
#   ErrorNotBashtoolsProject -> Si le projet n'est pas un bashtools projet
# Since: 2.0.0
##################################################################
function install_config_file ()
{
    # On récupère le chemin du projet
    path_project=${1:-}

    # On vérifie que le chemin existe bien
    directory_exist "${path_project:-}"

    # On vérifie que le projet est bien un projet bashtools
    is_bashtools_project "${path_project}"

    # On récupère les différentes informations du projet
    name_project=`get_param "name" ${path_project}/${FILE_BASHTOOLS}`

    # On vérifie que les informations obligatoire du projet ne sont pas null
    arg_is_not_null "${name_project:-}" "Don't find name variable in ${FILE_BASHTOOLS}."

    #On récupère le nom de l'utilisateur courant
    user=`whoami | awk -F "[[:blank:]]" '{print $1}'`

    # Nom du fichier de configuration
    filerc=.${name_project}rc

    # Si le fichier n'est pas déjà installé et n'est pas vide, on l'install dans le dossier courant de l'utilisateur
    if [[ ! -f /home/${user}/${filerc} ]] && grep -v "^[[:blank:]]*#" ${path_project}/${filerc} >> /dev/null
    then
        cp -v ${path_project}/${filerc} /home/${user}/
    fi

    return 0
}