#!/usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Affiche les projets installés
# Since: 2.0.0
##################################################################
function list ()
{
    # On vérifie que l'utilisateur est root
    user_is_root

    # On crée le dossier de sauvegarde par defaut
    create_save_direction

    # On liste tous les éléments contenus
    liste=`ls -F ${PATH_SAVE_DIRECTORY}`

    # On liste les éléments
    if [[ ! -z ${liste} ]]
    then
        echo "=================================="
        echo "      Elements installés : "
        echo "----------------------------------"
        echo ${liste}
        echo "=================================="

    else
        echo "=================================="
        echo "     Aucun élément installé"
        echo "=================================="
    fi

    exit 0
}