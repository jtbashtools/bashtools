#!/usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Création d'un nouveau projet
# Arguments:
#   $1 -> Nom du projet
#   $2 -> Chemin du dossier bin 
#   $3 -> Chemin du dossier lib
#   $4 -> Chemin du dossier data
#   $5 -> Version du projet
#   $6 -> Auteur du projet
#   $7 -> Description du projet
# Errors:
#   ErrorArgNull -> Lorsque l'un des arguments obligatoires est null.
# Since: 2.0.0
##################################################################
function create_new_project ()
{

    # On récupère les arguments
    name_project=${1:-}
    version_project=${2:-}
    auteur_project=${3:-}
    bin_project=${4:-}
    lib_project=${5:-}
    data_project=${6:-}
    description_project=${7:-}

    # On vérifie que les arguments obligatoire ne sont pas null
    arg_is_not_null "${name_project:-}" "name_project mustn't be null."
    arg_is_not_null "${version_project:-}" "verion_project mustn't be null."
    arg_is_not_null "${auteur_project:-}" "auteur_project mustn't be null."
    arg_is_not_null "${bin_project:-}" "bin_project mustn't be null."
    arg_is_not_null "${lib_project:-}" "lib_project mustn't be null."
    arg_is_not_null "${data_project:-}" "data_project mustn't be null."

    # On crée les dossiers du projets
    mkdir -v ${name_project}
    mkdir -v ${name_project}/bin
    mkdir -v ${name_project}/data
    mkdir -v ${name_project}/lib

    # On crée le fichier README
    echo "# ${name_project}" > ${name_project}/README.md

    # On crée le fichier des constantes
    echo "# Constantes du projet ${name_project}" > ${name_project}/${name_project}.const

    # On copie la librairie "util.sh"
    cp ${path_data}/util.sh ${name_project}/lib/

    # On crée et initialise le fichier bashtools.yml
    cp ${path_data}/${FILE_BASHTOOLS} ${name_project}/
    sed -i "s|\(^name:[[:space:]]\)\(name$\)|\1${name_project}|g" ${name_project}/${FILE_BASHTOOLS}
    sed -i "s|\(^version:[[:space:]]\)\(version$\)|\1${version_project}|g" ${name_project}/${FILE_BASHTOOLS}
    sed -i "s|\(^auteur:[[:space:]]\)\(auteur$\)|\1${auteur_project}|g" ${name_project}/${FILE_BASHTOOLS}
    sed -i "s|\(^description:[[:space:]]\)\(description$\)|\1${description_project:-}|g" ${name_project}/${FILE_BASHTOOLS}
    sed -i "s|\(^const_file:[[:space:]]\)\(const_file$\)|\1${name_project}.const|g" ${name_project}/${FILE_BASHTOOLS}
    sed -i "s|\(^[[:space:]]*bin:[[:space:]]\)\(bin$\)|\1${bin_project}|g" ${name_project}/${FILE_BASHTOOLS}
    sed -i "s|\(^[[:space:]]*lib:[[:space:]]\)\(lib$\)|\1${lib_project}|g" ${name_project}/${FILE_BASHTOOLS}
    sed -i "s|\(^[[:space:]]*data:[[:space:]]\)\(data$\)|\1${data_project}|g" ${name_project}/${FILE_BASHTOOLS}

    # On crée le fichier de script par défault
    cat ${path_data}/script.default > ${name_project}/bin/${name_project} && chmod +x ${name_project}/bin/${name_project}

    # On crée le fichier d'aide
    cat ${path_data}/script.help > ${name_project}/data/${name_project}.help

    # On crée le fichier de configuration
    echo "# Fichier de configuration (copiez le dans votre répertoir courant)" > ${name_project}/.${name_project}rc

    return 0
}
