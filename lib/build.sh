#!/usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Build un projet bashtools
# Arguments:
#   $1 -> Chemin du projet
# Error:
#   ErrorArgNull -> Si le chemin est null
#   ErrorNotExistDirectory -> Si le chemin n'éxiste pas
#   ErrorNotBashtoolsProject -> Si le projet n'est pas un bashtools projet
# Since: 2.0.0
##################################################################
function build_project ()
{

    # On récupère le chemin du projet
    path_project=${1:-}

    # On vérifie que le chemin existe bien
    directory_exist "${path_project:-}"

    # On vérifie que le projet est bien un projet bashtools
    is_bashtools_project "${path_project}"

    # On créer le fichier FILE_BUNDLE dans lib/ qui comporend toutes les constatntantes et le librairies
    add_legende "Variables d'environemments du projet" > ${path_project}/lib/${FILE_BUNDLE}
    parse_yml_file ${path_project}/${FILE_BASHTOOLS} >> ${path_project}/lib/${FILE_BUNDLE}
    # Saut de ligne afin d'éviter les pbs
    echo -e "\n" >> ${path_project}/lib/${FILE_BUNDLE}

    # On récupère le fichier de constante (si il existe)
    const_file=`get_param const_file ${path_project}/${FILE_BASHTOOLS}`

    # On ajoute les constantes du fichier de constante dans FILE_BUNDLE
    if [[ ! -z ${const_file:-} ]];
    then
        add_legende "Constantes du projet" >> ${path_project}/lib/${FILE_BUNDLE}
        cat ${path_project}/${const_file} >> ${path_project}/lib/${FILE_BUNDLE}
        # Saut de ligne afin d'éviter les pbs
        echo -e "\n" >> ${path_project}/lib/${FILE_BUNDLE}
    fi

    # On récupère le chemin des librairies
    path_lib=`get_param path_lib ${path_project}/${FILE_BASHTOOLS}`

    # On ajoute toutes les librairies
    list_file_lib=(`ls ${path_project}/lib/`)
    add_legende "Librairies du projets" >> ${path_project}/lib/${FILE_BUNDLE}
    for file_lib in ${list_file_lib[@]:-}
    do
        echo ". ${path_lib}/${file_lib}" >> ${path_project}/lib/${FILE_BUNDLE}
    done

    # On ajoute .bundle.sh dans tout les fichiers bins
    list_file_bin=(`ls ${path_project}/bin/`)
    for file_bin in ${list_file_bin[@]}
    do
        if grep "^\.[[:space:]].*${FILE_BUNDLE}" ${path_project}/bin/${file_bin} >> /dev/null
        then
            sed -i "s|^\.[[:space:]].*${FILE_BUNDLE}|\. ${path_lib}/${FILE_BUNDLE}|g" ${path_project}/bin/${file_bin}
        else
            sed -i "3i\\. ${path_lib}/${FILE_BUNDLE}" ${path_project}/bin/${file_bin}
        fi
    done

    return 0
}

##################################################################
# Purpose: Ajoute un commentaire de légende
# Arguments:
#   $1 -> Légende à afficher
# Error:
#   ErrorArgNull -> Si le chemin est null
# Since: 2.0.0
##################################################################
function add_legende ()
{
    legende=${1:-}
    arg_is_not_null "${legende:-}" "Legende mustn't be null."

    echo -e "##################################################################\n"\
    "# ${legende} \n"\
    "##################################################################"
    
    return 0
}