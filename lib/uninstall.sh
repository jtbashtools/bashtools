#!/usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Récupère le chemin du projet installé
# Arguments:
#   $1 -> Project name
# Return:
#   $project_directory -> Le chemin du projet installé
# Errors:
#   ErrorArgNull -> Si project name est null
#   ErrorNotInstalledProject -> Si le projet n'est pas installé
# Since: 2.0.0
##################################################################
function get_path_project_saved ()
{
    # On récupère le nom du projet
    project_name=${1:-}

    # Si aucun n'est indiqué et que le dossier courant est un projet bashtools, on récupère le nom du project
    if [[ -z ${project_name:-} && -f ./${FILE_BASHTOOLS} ]];
    then
        project_name=`get_param name ./${FILE_BASHTOOLS}`
    fi

    # On vérifie que le nom du projet n'est pas null
    arg_is_not_null "${project_name:-}" "Project name must'nt be null"

    # On vérifie que le projet est bien installé
    if ls ${PATH_SAVE_DIRECTORY} | egrep "^${project_name}@.*" >> /dev/null
    then
        echo "${PATH_SAVE_DIRECTORY}/`ls ${PATH_SAVE_DIRECTORY} | egrep \"^${project_name}@.*0\"`"
        return 0
    fi
        
    >&2 echo "ErrorNotInstalledProject: The project isn't installed"
    return 1

}

##################################################################
# Purpose: Désinstall un projet bashtools
# Arguments:
#   $1 -> Project directory
# Errors:
#   ErrorArgNull -> Si project directory ou d'autres variables sont null
#   ErrorNotExistDirectory -> Si le chemin de project directory est n'existe pas
# Since: 2.0.0
##################################################################
function uninstall_project ()
{
    # On récupère le chemin du projet sauvegardé
    project_directory=${1:-}

    # On vérifie que le projet est bien installé
    directory_exist "${project_directory:-}"

    # On récupère les différentes informations du projet
    version_project=`get_param version ${project_directory}/${FILE_BASHTOOLS}`
    path_bin_project=`get_param path_bin ${project_directory}/${FILE_BASHTOOLS}`
    path_lib_project=`get_param path_lib ${project_directory}/${FILE_BASHTOOLS}`
    path_data_project=`get_param path_data ${project_directory}/${FILE_BASHTOOLS}`
    uninstall_file=`get_param "uninstall_file" ${project_directory}/${FILE_BASHTOOLS}`

    # On vérifie que les informations obligatoire du projet ne sont pas null
    arg_is_not_null "${version_project:-}" "Don't find version variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${path_bin_project:-}" "Don't find path_bin_project variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${path_lib_project:-}" "Don't find path_lib_project variable in ${FILE_BASHTOOLS}."
    arg_is_not_null "${path_data_project:-}" "Don't find path_data_project variable in ${FILE_BASHTOOLS}."

    # On supprime les fichiers bins 
    bins_to_delete=(`cat ${project_directory}/${FILE_TO_UNINSTALL}`)
    for bin_to_delete in ${bins_to_delete[@]:-}
    do
        if [[ -f ${path_bin_project}/${bin_to_delete} ]]
        then
            rm -fv ${path_bin_project}/${bin_to_delete}
        fi
    done

    # On supprime le dossier lib
    if [[ -d ${path_lib_project} ]];
    then
        rm -rfv ${path_lib_project}
    fi

    # On supprime le dossier data
    if [[ -d ${path_data_project} ]];
    then
        rm -rfv ${path_data_project}
    fi
    
    # On execute le fichier uninstall supplémentaire si il existe
    if [[ -f ${uninstall_file:-} ]];
    then
        chmod +x ${project_directory}/${uninstall_file}
        cd ${project_directory} && ./${uninstall_file} && cd -
    fi

    return 0
}

##################################################################
# Purpose: Supprime le fichier de configuration d'un projet bashtools
# Arguments:
#   $1 -> Project directory
# Errors:
#   ErrorArgNull -> Si project directory ou d'autres variables sont null
#   ErrorNotExistDirectory -> Si le chemin de project directory est n'existe pas
# Since: 2.0.0
##################################################################
function delete_config_file ()
{
    # On récupère le chemin du projet sauvegardé
    project_directory=${1:-}

    # On vérifie que le projet est bien installé
    directory_exist "${project_directory:-}"

    # On récupère le nom du projet
    name_project=`get_param name ${project_directory}/${FILE_BASHTOOLS}`
    arg_is_not_null "${name_project:-}" "Don't find version variable in ${FILE_BASHTOOLS}."

    # On récupère le nom de l'utilisateur courant
    user=`whoami | awk -F "[[:blank:]]" '{print $1}'`
    arg_is_not_null "${user:-}" "User mustn't be null."

    if [[ -f /home/${user}/.${name_project}rc ]];
    then
        rm -fv /home/${user}/.${name_project}rc
    fi
    return 0
}

##################################################################
# Purpose: Supprime le dossier de sauvegarde d'un projet bashtools
# Arguments:
#   $1 -> Project directory
# Errors:
#   ErrorArgNull -> Si project directory est null
#   ErrorNotExistDirectory -> Si le chemin de project directory est n'existe pas
# Since: 2.0.0
##################################################################
function delete_save_direction ()
{
    # On récupère le chemin du projet sauvegardé
    project_directory=${1:-}

    # On vérifie que le projet est bien installé
    directory_exist "${project_directory:-}"

    # On supprime le repertoire de sauvegarde du projet
    rm -rfv ${project_directory}

    return 0
}
