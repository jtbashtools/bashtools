#!/usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Check if user is root
# Error:
#   ErrorNotRootUser -> if user doesn't root
# Since: 2.0.0
##################################################################
function user_is_root ()
{
    # On vérifie que l'utilisateur est root
    if [[ "$EUID" -ne 0 ]];
    then
        >&2 echo -e "ErrorNotRootUser: courant user doesn't root.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Check if arg is not null or not empty
# Arguments:
#   $1 -> Arg to check
#   $2 -> error message (must be null)
# Error:
#   ErrorArgNull -> if arg $1 is null
# Since: 2.0.0
##################################################################
function arg_is_not_null ()
{
    if [[ -z ${1:-} ]];
    then
        >&2 echo -e "ErrorArgNull : the arg is null or empty.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Check if directory exist
# Arguments:
#   $1 -> path of directory
#   $2 -> error message (must be null)
# Error:
#   ErrorNotExistDirectory -> if directory doesn't exist
#   ErrorArgNull -> if arg $1 is null
# Since: 2.0.0
##################################################################
function directory_exist ()
{
    arg_is_not_null "${1:-}" "Path is null."
    if [[ ! -d ${1} ]];
    then
        >&2 echo -e "ErrorNotExistDirectory: the direcory ${1} doesn't exist.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Check if file exist
# Arguments:
#   $1 -> path of file
#   $2 -> error message (must be null)
# Error:
#   ErrorNotExistFile -> if directory doesn't exist
#   ErrorArgNull -> if arg $1 is null
# Since: 2.0.0
##################################################################
function file_exist ()
{
    arg_is_not_null "${1:-}" "Path is null."
    if [[ ! -f ${1} ]];
    then
        >&2 echo -e "ErrorNotExistFile: the file ${1} doesn't exist.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Retourne le chemin du projet d'une liste d'arguments 
# Arguments:
#   $@ -> liste d'argument (peut être null)
# Return:
#   $path_project -> Le chemin du projet (par défault ./)
# Since: 2.0.0
##################################################################
function get_path_project ()
{
    for arg in ${@:-}
    do
        if [[ -d ${arg} || -d `pwd`/${arg} ]];
        then
            echo ${arg}
            return 0
        fi
    done
    echo "./"
    return 0
}


##################################################################
# Purpose: parse yaml file
# Arguments:
#   $1 -> path of yaml file (oligatoire)
# Error:
#   ErrorArgNull -> if arg $1 is null
# Since: 2.0.0
##################################################################
function parse_yml_file ()
{
    local path_file=${1:-}
    local s
    local w
    local fs

    arg_is_not_null "${path_file:-}" "Path of yaml file mustn't be null"

    s='[[:space:]]*'
    w='[a-zA-Z0-9_]*'
    fs="$(echo @|tr @ '\034'|tr -d '\015')"
    sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" "$1" |
    awk -F"$fs" '{
    indent = length($1)/2;
    vname[indent] = $2;
    for (i in vname) {if (i > indent) {delete vname[i]}}
        if (length($3) > 0) {
            vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
            printf("%s%s%s=(\"%s\")\n",vn, $2, $3);
        }
    }' | sed 's/_=/+=/g'
    return 0
}

##################################################################
# Purpose: Get the param's value
# Arguments:
#   $1 -> param's name
#   $2 -> path of yaml file
# Return:
#   $value -> the value of param
# Error:
#   ErrorArgNull -> if arg $1 or $2 are null
# Since: 2.0.0
##################################################################
function get_param ()
{
    arg_is_not_null "${1:-}" "Param mustn't be null"
    arg_is_not_null "${2:-}" "Path of yaml file mustn't be null"

    params=(`parse_yml_file ${2} | egrep "^${1}\+{0,1}="`)
    for param in ${params[@]:-}
    do
        echo ${param} | sed 's|\(^.*(\"\)\(.*\)\(\")\)|\2|g'
    done
    return 0
}
