#!/usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Check if user is root
# Error:
#   ErrorNotRootUser -> if user doesn't root
# Since: 2.0.0
##################################################################
function user_is_root ()
{
    # On vérifie que l'utilisateur est root
    if [[ "$EUID" -ne 0 ]];
    then
        >&2 echo -e "ErrorNotRootUser: courant user doesn't root.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Check if arg is not null or not empty
# Arguments:
#   $1 -> Arg to check
#   $2 -> error message (must be null)
# Error:
#   ErrorArgNull -> if arg $1 is null
# Since: 2.0.0
##################################################################
function arg_is_not_null ()
{
    if [[ -z ${1:-} ]];
    then
        >&2 echo -e "ErrorArgNull : the arg is null or empty.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Check if directory exist
# Arguments:
#   $1 -> path of directory
#   $2 -> error message (must be null)
# Error:
#   ErrorNotExistDirectory -> if directory doesn't exist
#   ErrorArgNull -> if arg $1 is null
# Since: 2.0.0
##################################################################
function directory_exist ()
{
    arg_is_not_null "${1:-}" "Path is null."
    if [[ ! -d ${1} ]];
    then
        >&2 echo -e "ErrorNotExistDirectory: the direcory ${1} doesn't exist.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Check if file exist
# Arguments:
#   $1 -> path of file
#   $2 -> error message (must be null)
# Error:
#   ErrorNotExistFile -> if directory doesn't exist
#   ErrorArgNull -> if arg $1 is null
# Since: 2.0.0
##################################################################
function file_exist ()
{
    arg_is_not_null "${1:-}" "Path is null."
    if [[ ! -f ${1} ]];
    then
        >&2 echo -e "ErrorNotExistFile: the file ${1} doesn't exist.\n${2:-}"
        return 1
    fi
    return 0
}

##################################################################
# Purpose: Check if a bashtools project
# Arguments:
#   $1 -> path of project
#   $2 -> error message (must be null)
# Errors:
#   ErrorArgNull -> if $1 is null
#   ErrorNotExistDirectory -> if $1 isn't a directory
#   ErrorNotBashtoolsProject -> if $1 isn't a bashtools project
# Since: 2.0.0
##################################################################
function is_bashtools_project ()
{
    path_project=${1:-}

    arg_is_not_null "${path_project}" "Path_project is null."

    directory_exist "${path_project}" "Path_project isn't a directory"

    if [[ ! -f ${path_project}/bashtools.yml ]];
    then
        >&2 echo -e "ErrorNotBashtoolsProject: ${path_project} isn't a bashtools project.\n${2:-}"    
        return 1
    fi
    return 0
}