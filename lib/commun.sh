#!/usr/bin/env bash

set -o nounset
set -o errexit

##################################################################
# Purpose: Retourne le chemin du projet d'une liste d'arguments 
# Arguments:
#   $@ -> liste d'argument (peut être null)
# Return:
#   $path_project -> Le chemin du projet (par défault ./)
# Since: 2.0.0
##################################################################
function get_path_project ()
{
    for arg in ${@:-}
    do
        if [[ -d ${arg} || -d `pwd`/${arg} ]];
        then
            echo ${arg}
            return 0
        fi
    done
    echo "./"
    return 0
}

##################################################################
# Purpose: Retourne le mode du projet d'une liste d'arguements
# Arguments:
#   $@ -> liste d'argument (peut être null)
# Return:
#   $mode -> Le mode du projet (dev ou prod, default prod)
# Since: 2.0.0
##################################################################
function get_mode_project () 
{
    if echo ${@:-} | grep "\-\-dev" >> /dev/null \
        || echo ${@:-} | grep "\-d " >> /dev/null
    then
        echo "dev"
        return 0
    fi

    echo "prod"
    return 0
}

##################################################################
# Purpose: Créer le dossier de sauvegarde des projets bashtools
# Since: 2.0.0
##################################################################
function create_save_direction ()
{
    mkdir -p ${PATH_SAVE_DIRECTORY}
    return 0
}

##################################################################
# Purpose: parse yaml file
# Arguments:
#   $1 -> path of yaml file (default ./bashtools.yml)
#   $2 -> suffix
# Since: 2.0.0
##################################################################
function parse_yml_file ()
{
    local path_file=${1:-}
    local prefix=${2:-}
    local s
    local w
    local fs

    if [[ -z ${path_file:-} ]];
    then
        path_file="./bashtools.yml"
    fi

    s='[[:space:]]*'
    w='[a-zA-Z0-9_]*'
    fs="$(echo @|tr @ '\034'|tr -d '\015')"
    sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" "$1" |
    awk -F"$fs" '{
    indent = length($1)/2;
    vname[indent] = $2;
    for (i in vname) {if (i > indent) {delete vname[i]}}
        if (length($3) > 0) {
            vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
            printf("%s%s%s=(\"%s\")\n", "'"${prefix:-}"'",vn, $2, $3);
        }
    }' | sed 's/_=/+=/g'
    return 0
}

##################################################################
# Purpose: Get the param's value
# Arguments:
#   $1 -> param's name
#   $2 -> path of yaml file (default ./bashtools.yml)
# Return:
#   $value -> the value of param
# Since: 2.0.0
##################################################################
function get_param ()
{
    arg_is_not_null "${1:-}"
    params=(`parse_yml_file ${2:-} | egrep "^${1}\+{0,1}="`)
    for param in ${params[@]:-}
    do
        echo ${param} | sed 's|\(^.*(\"\)\(.*\)\(\")\)|\2|g'
    done
    return 0
}