##################################################################
 # Variables d'environemments du projet 
 ##################################################################
name=("bashtools")
version=("2.0.0-beta")
auteur=("jtutzo")
description=("Une description")
const_file=("bashtools.const")
path_bin=("/usr/local/bin")
path_data=("/usr/local/share/bashtools")
path_lib=("/usr/local/lib/bashtools")
dependances+=("git")


##################################################################
 # Constantes du projet 
 ##################################################################
# Nom du fichier de configuration des projets bashtools
FILE_BASHTOOLS=bashtools.yml

# Nom du fichier bundle
FILE_BUNDLE=.bundle.sh

# Nom du fichier contenant les fichiers bin à supprimer lors de la désinstallation
FILE_TO_UNINSTALL=file_to_uninstall

# Chemin ou sont sauvegardé les projets installés
PATH_SAVE_DIRECTORY=~/.bashtools

##################################################################
 # Librairies du projets 
 ##################################################################
. /usr/local/lib/bashtools/build.sh
. /usr/local/lib/bashtools/commun.sh
. /usr/local/lib/bashtools/install.sh
. /usr/local/lib/bashtools/list.sh
. /usr/local/lib/bashtools/new.sh
. /usr/local/lib/bashtools/precondition.sh
. /usr/local/lib/bashtools/uninstall.sh
