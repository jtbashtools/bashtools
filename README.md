# bashtools
> Bashtools permet la création, l'installation et la désinstallation de script bash.

<BR />

## Installation/désinstallation de bashtools

Récupérez les source avec git
```bash
git clone https://gitlab.com/jtutzo/bashtools.git
```
Installez la commande (en mode production)
```bash
cd bashtools/ && sudo ./install
```

Désinstallez la commande
```bash
cd bashtools/ && sudo ./uninstall
```
<BR />

## Commandes de base

Créer un projet bash `demo`
```bash
bashtools new demo
```

Installer le projet `demo`
```bash
cd demo && sudo bashtools install
```

Désinstaller le projet `demo`
```bash
cd demo && sudo bashtools uninstall
```

Lister les projets installés
```bash
bashtools list
```
